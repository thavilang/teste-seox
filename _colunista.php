<div class="colunista">
    <div class="colunista__img">
        <img src="assets/images/colunista.png" alt="Imagem de perfil: Felipe Bittencourt">
        <h3>Felipe Bittencourt</h3>
    </div>
    <div class="colunista__content">
        <h4 class="colunista__titulo">Data cap é o novo market cap</h4>
        <p class="colunista__subtitulo">A certificação de dados desestimula vazamentos de informações</p>
        <p class="colunista__texto">Deep tech brasileira criou um método para reciclar ondas eletromagnéticas dispersas no ar como fonte de energia para dispositivos IoT. </p>
    </div>
</div>