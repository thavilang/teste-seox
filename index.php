<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/index.css">
    <script src="assets/js/plugins/swiper.min.js"></script>
    <meta name="theme-color" content="#222222">
    <title>Teste Seox</title>
</head>

<body>
    <main>
        <h1 class="hidden">Teste Seox</h1>
        <?php 
        include '_videos.php';
        include '_colunistas-carousel.php'; 
        ?>
    </main>
</body>

</html>