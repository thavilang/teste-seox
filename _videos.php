<section class="videos dark">
    <div class="container">
        <div class="linha-titulo-navegacao mb-32">
            <div class="row align-items-center justify-content-between">
                <div class="col-auto">
                    <h2 class="linha-titulo-navegacao__titulo">VÍDEOS</h2>
                </div>
                <div class="col-auto">
                    <div class="row g-3">
                        <div class="d-none d-md-block col-auto">
                            <a href="#" class="padrao-botao">
                                VEJA MAIS
                                <svg>
                                    <use xlink:href="assets/images/sprite.svg#icon-seta" />
                                </svg>
                            </a>
                        </div>
                        <div class="col-auto">
                            <div class="swiper-navegacao vertical">
                                <button class="d-none d-sm-flex swiper-button-videos-prev prev"><svg>
                                        <use xlink:href="assets/images/sprite.svg#icon-seta" />
                                    </svg></button>
                                <button class="swiper-button-videos-next"><svg>
                                        <use xlink:href="assets/images/sprite.svg#icon-seta" />
                                    </svg></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-xxl-9 post-destacado">
                <?php include '_post.php' ?>
            </div>
            <div class="col-lg-4 col-xxl-3">
                <div js-swiper-videos class="swiper-posts-vertical swiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide"><?php include '_post.php' ?></div>
                        <div class="swiper-slide"><?php include '_post.php' ?></div>
                        <div class="swiper-slide"><?php include '_post.php' ?></div>
                        <div class="swiper-slide"><?php include '_post.php' ?></div>
                        <div class="swiper-slide"><?php include '_post.php' ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var swiper = new Swiper("[js-swiper-videos]", {
        slidesPerView: 1.2,
        spaceBetween: 24,
        navigation: {
            nextEl: ".swiper-button-videos-next",
            prevEl: ".swiper-button-videos-prev",
        },
        breakpoints: {
            992: {
                direction: 'vertical',
                slidesPerView: 2,
                spaceBetween: 20,
            }
        },
    });
</script>