Oi :)

Tentei estruturar todos os blocos em componentes independetes. O css de cada componentes está dentro de um arquivo _componente.scss
com o mesmo nome do include, é necessário adicionar o css de cada componente no arquivo scss da página desejada.

A postagem possui uma versão light apesar de não ter sido mostrada no layout porque, por se tratar de um componente de post, eu supus que em um possível projeto com esse layout posts com fundo cinza claro seriam necessários em algum momento. As cores são trocadas com variaveis css.

Não consegui deixar o slide dos colunistas redondo, faltou fazer ele voltar para a posição inicial no mouseleave (não deu tempo)