<section class="colunistas">
    <div class="container">
        <div class="linha-titulo-navegacao mb-3">
            <div class="row align-items-center justify-content-between">
                <div class="col-auto">
                    <h2 class="linha-titulo-navegacao__titulo">COLUNISTAS</h2>
                </div>
                <div class="col-auto">
                    <div class="swiper-navegacao">
                        <button class="swiper-button-colunistas-prev prev d-none d-sm-flex">
                            <svg>
                                <use xlink:href="assets/images/sprite.svg#icon-seta" />
                            </svg>
                        </button>
                        <button class="swiper-button-colunistas-next">
                            <svg>
                                <use xlink:href="assets/images/sprite.svg#icon-seta" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div js-swiper-colunistas class="swiper-posts-vertical swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
                <div class="swiper-slide"><?php include '_colunista.php' ?></div>
            </div>
        </div>
    </div>
</section>

<script>
    let colunistasSlide = document.querySelector('[js-swiper-colunistas]');
    colunistasSlide.querySelector('.swiper-wrapper').innerHTML = colunistasSlide.querySelector('.swiper-wrapper').innerHTML + '<div class="d-none d-lg-block swiper-slide"></div> <div class="d-none d-lg-block swiper-slide"></div>';
    let swiperColunistas = new Swiper("[js-swiper-colunistas]", {
        slidesPerView: "auto",
        spaceBetween: 24,
        navigation: {
            nextEl: ".swiper-button-colunistas-next",
            prevEl: ".swiper-button-colunistas-prev",
        },
    });

    let slideAtual = 0;
    if (window.innerWidth >= 992) {
        let numSlides = 4;
        if (window.innerWidth >= 1380) {
            numSlides = 6;
        } else if (window.innerWidth >= 1200) {
            numSlides = 5;
        }

        let colunistasSlideSlide = colunistasSlide.querySelectorAll('.swiper-slide');

        colunistasSlideSlide.forEach((elemento, index) => {
            elemento.addEventListener('mouseover', function() {
                slideAtual = swiperColunistas.activeIndex;
                if (index == slideAtual + (numSlides - 1)) {
                    setTimeout(() => {
                        swiperColunistas.slideTo(slideAtual + 2);
                    }, 200);
                } else if (index == slideAtual + (numSlides - 2)) {
                    setTimeout(() => {
                        swiperColunistas.slideTo(slideAtual + 1);
                    }, 200);
                }
            });
            console.log(slideAtual);
            elemento.addEventListener('mouseleave', function() {
                setTimeout(() => {
                    swiperColunistas.slideTo(slideAtual);
                }, 250);
            });
        });
    }
</script>